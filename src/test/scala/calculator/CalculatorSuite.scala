package calculator

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import org.scalatest._

import TweetLength.MaxTweetLength

@RunWith(classOf[JUnitRunner])
class CalculatorSuite extends FunSuite with ShouldMatchers {

  /******************
   ** TWEET LENGTH **
   ******************/

  def tweetLength(text: String): Int =
    text.codePointCount(0, text.length)

  test("tweetRemainingCharsCount with a constant signal") {
    val result = TweetLength.tweetRemainingCharsCount(Var("hello world"))
    assert(result() == MaxTweetLength - tweetLength("hello world"))

    val tooLong = "foo" * 200
    val result2 = TweetLength.tweetRemainingCharsCount(Var(tooLong))
    assert(result2() == MaxTweetLength - tweetLength(tooLong))
  }

  test("tweetRemainingCharsCount with a supplementary char") {
    val result = TweetLength.tweetRemainingCharsCount(Var("foo blabla \uD83D\uDCA9 bar"))
    assert(result() == MaxTweetLength - tweetLength("foo blabla \uD83D\uDCA9 bar"))
  }


  test("colorForRemainingCharsCount with a constant signal") {
    val resultGreen1 = TweetLength.colorForRemainingCharsCount(Var(52))
    assert(resultGreen1() == "green")
    val resultGreen2 = TweetLength.colorForRemainingCharsCount(Var(15))
    assert(resultGreen2() == "green")

    val resultOrange1 = TweetLength.colorForRemainingCharsCount(Var(12))
    assert(resultOrange1() == "orange")
    val resultOrange2 = TweetLength.colorForRemainingCharsCount(Var(0))
    assert(resultOrange2() == "orange")

    val resultRed1 = TweetLength.colorForRemainingCharsCount(Var(-1))
    assert(resultRed1() == "red")
    val resultRed2 = TweetLength.colorForRemainingCharsCount(Var(-5))
    assert(resultRed2() == "red")
  }

  test("test 1") {
    val testMap: Map[String, Signal[Expr]] = Map(
      "a" -> Var(Literal(1)),
      "b" -> Var(Plus(Literal(6), Literal(3))),
      "c" -> Var(Minus(Literal(6), Literal(3))),
      "d" -> Var(Times(Literal(6), Literal(3))),
      "e" -> Var(Divide(Literal(6), Literal(3))),
      "f" -> Var(Ref("a")),
      "g" -> Var(Plus(Ref("a"), Literal(4)))
    )

    val resultMap: Map[String, Double] = Map(
      "a" -> 1D,
      "b" -> 9D,
      "c" -> 3D,
      "d" -> 18D,
      "e" -> 2D,
      "f" -> 1D,
      "g" -> 5D
    )

    val computeResult = Calculator.computeValues(testMap)

    computeResult foreach { result =>
      println(s"var ${result._1} has value ${result._2()}")
    }

    computeResult foreach { element =>
      assert( resultMap(element._1) == element._2() )
    }
  }

}
